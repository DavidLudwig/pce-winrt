/*
  David's SDL Utility Library (DSUL)
  Copyright (C) 2013 David Ludwig <dludwig@pobox.com>

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#include "DSUL.h"

static int SDLCALL DSUL_EventWatch(void *userdata, SDL_Event * event);

static Uint32 video_flags = 0;
static SDL_Surface * inner_video_surface = NULL;
static SDL_Surface * outer_video_surface = NULL;
static SDL_Window * video_window = NULL;
static SDL_Renderer * video_renderer = NULL;
static SDL_Texture * video_texture = NULL;

extern "C" SDL_bool
DSUL_IsVideoModeSet()
{
    return (outer_video_surface != NULL) ? SDL_TRUE : SDL_FALSE;
}

static void
DSUL_ShutdownVideo()
{
    if (video_texture) {
        SDL_DestroyTexture(video_texture);
        video_texture = NULL;
    }

    if (video_renderer) {
        SDL_DestroyRenderer(video_renderer);
        video_renderer = NULL;
    }

    if (inner_video_surface) {
        if (inner_video_surface == outer_video_surface) {
            outer_video_surface = NULL;
        }

        SDL_FreeSurface(inner_video_surface);
        inner_video_surface = NULL;
    }

    if (outer_video_surface) {
        SDL_FreeSurface(outer_video_surface);
        outer_video_surface = NULL;
    }

    if (video_window) {
        SDL_DestroyWindow(video_window);
        video_window = NULL;
    }

    SDL_DelEventWatch(DSUL_EventWatch, NULL);

    video_flags = 0;
}

extern "C" SDL_Surface *
DSUL_SetVideoMode(int width, int height, int bpp, Uint32 flags)
{
    if (DSUL_IsVideoModeSet()) {
        DSUL_ShutdownVideo();
    }

    SDL_DisplayMode display_mode;
#ifdef __WIN32__
    SDL_zero(display_mode);
    display_mode.w = 512;
    display_mode.h = 384;
#else
    if (SDL_GetDesktopDisplayMode(0, &display_mode) != 0) {
        DSUL_ShutdownVideo();
        return NULL;
    }
#endif

    Uint32 window_flags = 0;
#ifndef __WIN32__
    if (flags & DSUL_FULLSCREEN) {
        window_flags |= SDL_WINDOW_FULLSCREEN;
    }
#endif
    if (flags & DSUL_RESIZABLE) {
        window_flags |= SDL_WINDOW_RESIZABLE;
    }

    video_window = SDL_CreateWindow(
        "DSUL Window",
        SDL_WINDOWPOS_UNDEFINED,
        SDL_WINDOWPOS_UNDEFINED,
        display_mode.w,
        display_mode.h,
        window_flags);
    if (!video_window) {
        DSUL_ShutdownVideo();
        return NULL;
    }

    video_renderer = SDL_CreateRenderer(video_window, -1, SDL_RENDERER_ACCELERATED);
    if (!video_renderer) {
        DSUL_ShutdownVideo();
        return NULL;
    }

    video_texture = SDL_CreateTexture(video_renderer, SDL_PIXELFORMAT_RGB888, SDL_TEXTUREACCESS_STREAMING, width, height);
    if (!video_texture) {
        DSUL_ShutdownVideo();
        return NULL;
    }

    switch (bpp) {
        case 8:
            outer_video_surface = SDL_CreateRGBSurface(0, width, height, 8, 0, 0, 0, 0);
            break;
        case 32:
            outer_video_surface = SDL_CreateRGBSurface(0, width, height, 32, 0x00FF0000, 0x0000FF00, 0x000000FF, 0xFF000000);
            inner_video_surface = outer_video_surface;
            break;
        default:
            SDL_SetError("DSUL_SetVideoMode: An unsupported bpp of %d was specified", bpp);
            break;
    }
    if (!outer_video_surface) {
        DSUL_ShutdownVideo();
        return NULL;
    }

    if (!inner_video_surface) {
        inner_video_surface = SDL_CreateRGBSurface(0, width, height, 32, 0x00FF0000, 0x0000FF00, 0x000000FF, 0xFF000000);
        if (!inner_video_surface) {
            DSUL_ShutdownVideo();
            return NULL;
        }
    }
    video_flags = flags;

    SDL_AddEventWatch(DSUL_EventWatch, NULL);

    return outer_video_surface;
}

extern "C" Uint32
DSUL_GetVideoModeFlags()
{
    return video_flags;
}

extern "C" SDL_Surface *
DSUL_GetVideoSurface()
{
    return outer_video_surface;
}

extern "C" SDL_Window *
DSUL_GetVideoWindow()
{
    return video_window;
}

extern "C" SDL_Renderer *
DSUL_GetVideoRenderer()
{
    return video_renderer;
}

extern "C" int
DSUL_Flip()
{
    if (!video_renderer || !video_texture || !outer_video_surface) {
        SDL_SetError("DSUL: A video mode does not appear to have been set.");
        return -1;
    }

    if (inner_video_surface != outer_video_surface &&
        inner_video_surface != NULL &&
        outer_video_surface != NULL)
    {
        SDL_Rect destRect = {0, 0, inner_video_surface->w, inner_video_surface->h};
        if (SDL_BlitSurface(outer_video_surface, NULL, inner_video_surface, &destRect) != 0) {
            return -1;
        }
    }

    if (SDL_UpdateTexture(video_texture, NULL, inner_video_surface->pixels, inner_video_surface->pitch) != 0) {
        return -1;
    }

    SDL_Rect src_rect = {0, 0, inner_video_surface->w, inner_video_surface->h};
    if (SDL_RenderCopy(video_renderer, video_texture, &src_rect, NULL) != 0) {
        return -1;
    }

    SDL_RenderPresent(video_renderer);

    return 0;
}

static int SDLCALL
DSUL_EventWatch(void *userdata, SDL_Event * event)
{
    if (!inner_video_surface || !video_window) {
        return -1;
    }

    int window_width = 0;
    int window_height = 0;
    SDL_GetWindowSize(video_window, &window_width, &window_height);
    if (window_width == 0 || window_height == 0) {
        return -1;
    }

    float scale_x = (float)inner_video_surface->w / (float)window_width;
    float scale_y = (float)inner_video_surface->h / (float)window_height;

    switch (event->type) {
        case SDL_MOUSEBUTTONDOWN:
        case SDL_MOUSEBUTTONUP:
            event->button.x = (int)(event->button.x * scale_x);
            event->button.y = (int)(event->button.y * scale_y);
            break;

        case SDL_MOUSEMOTION:
            event->motion.x = (int)(event->motion.x * scale_x);
            event->motion.y = (int)(event->motion.y * scale_y);
            event->motion.xrel = (int)(event->motion.xrel * scale_x);
            event->motion.yrel = (int)(event->motion.yrel * scale_y);
            break;

        // TODO: handle window events, such as those involving resizing
    }
    return 1;
}
