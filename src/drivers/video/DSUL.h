/*
  David's SDL Utility Library (DSUL)
  Copyright (C) 2013 David Ludwig <dludwig@pobox.com>

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#ifndef _DSUL_h
#define _DSUL_h

#if defined(__cplusplus)
extern "C" {
#endif

#include "SDL.h"

typedef enum
{
    DSUL_FULLSCREEN = 0x00000001,
    DSUL_RESIZABLE = 0x00000002
} DSUL_VideoFlags;

SDL_bool DSUL_IsVideoModeSet();
SDL_Surface * DSUL_SetVideoMode(int width, int height, int bpp, Uint32 flags);
Uint32 DSUL_GetVideoModeFlags();
SDL_Surface * DSUL_GetVideoSurface();
SDL_Window * DSUL_GetVideoWindow();
SDL_Renderer * DSUL_GetVideoRenderer();
int DSUL_Flip();

#if defined(__cplusplus)
}
#endif

#endif /* _DSUL_h */
